const options = [
  {
    id: '16',
    label: '河南省',
    value: '410000',
    children: [
      {
        id: '1176',
        label: '郑州市',
        value: '410100',
        children: [
          {
            id: '1547',
            label: '中原区',
            value: '410102'
          },
          {
            id: '1548',
            label: '二七区',
            value: '410103'
          },
          {
            id: '1549',
            label: '管城回族区',
            value: '410104'
          },
          {
            id: '1550',
            label: '金水区',
            value: '410105'
          },
          {
            id: '1551',
            label: '上街区',
            value: '410106'
          },
          {
            id: '1552',
            label: '惠济区',
            value: '410108'
          },
          {
            id: '1553',
            label: '中牟县',
            value: '410122'
          },
          {
            id: '1554',
            label: '巩义市',
            value: '410181'
          },
          {
            id: '1555',
            label: '荥阳市',
            value: '410182'
          },
          {
            id: '1556',
            label: '新密市',
            value: '410183'
          },
          {
            id: '1557',
            label: '新郑市',
            value: '410184'
          },
          {
            id: '1558',
            label: '登封市',
            value: '410185'
          }
        ]
      },
      {
        id: '1177',
        label: '开封市',
        value: '410200',
        children: [
          {
            id: '1560',
            label: '龙亭区',
            value: '410202'
          },
          {
            id: '1561',
            label: '顺河回族区',
            value: '410203'
          },
          {
            id: '1562',
            label: '鼓楼区',
            value: '410204'
          },
          {
            id: '1563',
            label: '禹王台区',
            value: '410205'
          },
          {
            id: '1564',
            label: '金明区',
            value: '410211'
          },
          {
            id: '1565',
            label: '杞县',
            value: '410221'
          },
          {
            id: '1566',
            label: '通许县',
            value: '410222'
          },
          {
            id: '1567',
            label: '尉氏县',
            value: '410223'
          },
          {
            id: '1568',
            label: '开封县',
            value: '410224'
          },
          {
            id: '1569',
            label: '兰考县',
            value: '410225'
          }
        ]
      },
      {
        id: '1178',
        label: '洛阳市',
        value: '410300',
        children: [
          {
            id: '1571',
            label: '老城区',
            value: '410302'
          },
          {
            id: '1572',
            label: '西工区',
            value: '410303'
          },
          {
            id: '1573',
            label: '瀍河回族区',
            value: '410304'
          },
          {
            id: '1574',
            label: '涧西区',
            value: '410305'
          },
          {
            id: '1575',
            label: '吉利区',
            value: '410306'
          },
          {
            id: '1576',
            label: '洛龙区',
            value: '410311'
          },
          {
            id: '1577',
            label: '孟津县',
            value: '410322'
          },
          {
            id: '1578',
            label: '新安县',
            value: '410323'
          },
          {
            id: '1579',
            label: '栾川县',
            value: '410324'
          },
          {
            id: '1580',
            label: '嵩县',
            value: '410325'
          },
          {
            id: '1581',
            label: '汝阳县',
            value: '410326'
          },
          {
            id: '1582',
            label: '宜阳县',
            value: '410327'
          },
          {
            id: '1583',
            label: '洛宁县',
            value: '410328'
          },
          {
            id: '1584',
            label: '伊川县',
            value: '410329'
          },
          {
            id: '1585',
            label: '偃师市',
            value: '410381'
          }
        ]
      },
      {
        id: '1179',
        label: '平顶山市',
        value: '410400',
        children: [
          {
            id: '1587',
            label: '新华区',
            value: '410402'
          },
          {
            id: '1588',
            label: '卫东区',
            value: '410403'
          },
          {
            id: '1589',
            label: '石龙区',
            value: '410404'
          },
          {
            id: '1590',
            label: '湛河区',
            value: '410411'
          },
          {
            id: '1591',
            label: '宝丰县',
            value: '410421'
          },
          {
            id: '1592',
            label: '叶县',
            value: '410422'
          },
          {
            id: '1593',
            label: '鲁山县',
            value: '410423'
          },
          {
            id: '1594',
            label: '郏县',
            value: '410425'
          },
          {
            id: '1595',
            label: '舞钢市',
            value: '410481'
          },
          {
            id: '1596',
            label: '汝州市',
            value: '410482'
          }
        ]
      },
      {
        id: '1180',
        label: '安阳市',
        value: '410500',
        children: [
          {
            id: '1598',
            label: '文峰区',
            value: '410502'
          },
          {
            id: '1599',
            label: '北关区',
            value: '410503'
          },
          {
            id: '1600',
            label: '殷都区',
            value: '410505'
          },
          {
            id: '1601',
            label: '龙安区',
            value: '410506'
          },
          {
            id: '1602',
            label: '安阳县',
            value: '410522'
          },
          {
            id: '1603',
            label: '汤阴县',
            value: '410523'
          },
          {
            id: '1604',
            label: '滑县',
            value: '410526'
          },
          {
            id: '1605',
            label: '内黄县',
            value: '410527'
          },
          {
            id: '1606',
            label: '林州市',
            value: '410581'
          }
        ]
      },
      {
        id: '1181',
        label: '鹤壁市',
        value: '410600',
        children: [
          {
            id: '1608',
            label: '鹤山区',
            value: '410602'
          },
          {
            id: '1609',
            label: '山城区',
            value: '410603'
          },
          {
            id: '1610',
            label: '淇滨区',
            value: '410611'
          },
          {
            id: '1611',
            label: '浚县',
            value: '410621'
          },
          {
            id: '1612',
            label: '淇县',
            value: '410622'
          }
        ]
      },
      {
        id: '1182',
        label: '新乡市',
        value: '410700',
        children: [
          {
            id: '1614',
            label: '红旗区',
            value: '410702'
          },
          {
            id: '1615',
            label: '卫滨区',
            value: '410703'
          },
          {
            id: '1616',
            label: '凤泉区',
            value: '410704'
          },
          {
            id: '1617',
            label: '牧野区',
            value: '410711'
          },
          {
            id: '1618',
            label: '新乡县',
            value: '410721'
          },
          {
            id: '1619',
            label: '获嘉县',
            value: '410724'
          },
          {
            id: '1620',
            label: '原阳县',
            value: '410725'
          },
          {
            id: '1621',
            label: '延津县',
            value: '410726'
          },
          {
            id: '1622',
            label: '封丘县',
            value: '410727'
          },
          {
            id: '1623',
            label: '长垣县',
            value: '410728'
          },
          {
            id: '1624',
            label: '卫辉市',
            value: '410781'
          },
          {
            id: '1625',
            label: '辉县市',
            value: '410782'
          }
        ]
      },
      {
        id: '1183',
        label: '焦作市',
        value: '410800',
        children: [
          {
            id: '1627',
            label: '解放区',
            value: '410802'
          },
          {
            id: '1628',
            label: '中站区',
            value: '410803'
          },
          {
            id: '1629',
            label: '马村区',
            value: '410804'
          },
          {
            id: '1630',
            label: '山阳区',
            value: '410811'
          },
          {
            id: '1631',
            label: '修武县',
            value: '410821'
          },
          {
            id: '1632',
            label: '博爱县',
            value: '410822'
          },
          {
            id: '1633',
            label: '武陟县',
            value: '410823'
          },
          {
            id: '1634',
            label: '温县',
            value: '410825'
          },
          {
            id: '1635',
            label: '沁阳市',
            value: '410882'
          },
          {
            id: '1636',
            label: '孟州市',
            value: '410883'
          }
        ]
      },
      {
        id: '1184',
        label: '濮阳市',
        value: '410900',
        children: [
          {
            id: '1638',
            label: '华龙区',
            value: '410902'
          },
          {
            id: '1639',
            label: '清丰县',
            value: '410922'
          },
          {
            id: '1640',
            label: '南乐县',
            value: '410923'
          },
          {
            id: '1641',
            label: '范县',
            value: '410926'
          },
          {
            id: '1642',
            label: '台前县',
            value: '410927'
          },
          {
            id: '1643',
            label: '濮阳县',
            value: '410928'
          }
        ]
      },
      {
        id: '1185',
        label: '许昌市',
        value: '411000',
        children: [
          {
            id: '1645',
            label: '魏都区',
            value: '411002'
          },
          {
            id: '1646',
            label: '许昌县',
            value: '411023'
          },
          {
            id: '1647',
            label: '鄢陵县',
            value: '411024'
          },
          {
            id: '1648',
            label: '襄城县',
            value: '411025'
          },
          {
            id: '1649',
            label: '禹州市',
            value: '411081'
          },
          {
            id: '1650',
            label: '长葛市',
            value: '411082'
          }
        ]
      },
      {
        id: '1186',
        label: '漯河市',
        value: '411100',
        children: [
          {
            id: '1652',
            label: '源汇区',
            value: '411102'
          },
          {
            id: '1653',
            label: '郾城区',
            value: '411103'
          },
          {
            id: '1654',
            label: '召陵区',
            value: '411104'
          },
          {
            id: '1655',
            label: '舞阳县',
            value: '411121'
          },
          {
            id: '1656',
            label: '临颍县',
            value: '411122'
          }
        ]
      },
      {
        id: '1187',
        label: '三门峡市',
        value: '411200',
        children: [
          {
            id: '1658',
            label: '湖滨区',
            value: '411202'
          },
          {
            id: '1659',
            label: '渑池县',
            value: '411221'
          },
          {
            id: '1660',
            label: '陕县',
            value: '411222'
          },
          {
            id: '1661',
            label: '卢氏县',
            value: '411224'
          },
          {
            id: '1662',
            label: '义马市',
            value: '411281'
          },
          {
            id: '1663',
            label: '灵宝市',
            value: '411282'
          }
        ]
      },
      {
        id: '1188',
        label: '南阳市',
        value: '411300',
        children: [
          {
            id: '1665',
            label: '宛城区',
            value: '411302'
          },
          {
            id: '1666',
            label: '卧龙区',
            value: '411303'
          },
          {
            id: '1667',
            label: '南召县',
            value: '411321'
          },
          {
            id: '1668',
            label: '方城县',
            value: '411322'
          },
          {
            id: '1669',
            label: '西峡县',
            value: '411323'
          },
          {
            id: '1670',
            label: '镇平县',
            value: '411324'
          },
          {
            id: '1671',
            label: '内乡县',
            value: '411325'
          },
          {
            id: '1672',
            label: '淅川县',
            value: '411326'
          },
          {
            id: '1673',
            label: '社旗县',
            value: '411327'
          },
          {
            id: '1674',
            label: '唐河县',
            value: '411328'
          },
          {
            id: '1675',
            label: '新野县',
            value: '411329'
          },
          {
            id: '1676',
            label: '桐柏县',
            value: '411330'
          },
          {
            id: '1677',
            label: '邓州市',
            value: '411381'
          }
        ]
      },
      {
        id: '1189',
        label: '商丘市',
        value: '411400',
        children: [
          {
            id: '1679',
            label: '梁园区',
            value: '411402'
          },
          {
            id: '1680',
            label: '睢阳区',
            value: '411403'
          },
          {
            id: '1681',
            label: '民权县',
            value: '411421'
          },
          {
            id: '1682',
            label: '睢县',
            value: '411422'
          },
          {
            id: '1683',
            label: '宁陵县',
            value: '411423'
          },
          {
            id: '1684',
            label: '柘城县',
            value: '411424'
          },
          {
            id: '1685',
            label: '虞城县',
            value: '411425'
          },
          {
            id: '1686',
            label: '夏邑县',
            value: '411426'
          },
          {
            id: '1687',
            label: '永城市',
            value: '411481'
          }
        ]
      },
      {
        id: '1190',
        label: '信阳市',
        value: '411500',
        children: [
          {
            id: '1689',
            label: '?负忧?',
            value: '411502'
          },
          {
            id: '1690',
            label: '平桥区',
            value: '411503'
          },
          {
            id: '1691',
            label: '罗山县',
            value: '411521'
          },
          {
            id: '1692',
            label: '光山县',
            value: '411522'
          },
          {
            id: '1693',
            label: '新县',
            value: '411523'
          },
          {
            id: '1694',
            label: '商城县',
            value: '411524'
          },
          {
            id: '1695',
            label: '固始县',
            value: '411525'
          },
          {
            id: '1696',
            label: '潢川县',
            value: '411526'
          },
          {
            id: '1697',
            label: '淮滨县',
            value: '411527'
          },
          {
            id: '1698',
            label: '息县',
            value: '411528'
          }
        ]
      },
      {
        id: '1191',
        label: '周口市',
        value: '411600',
        children: [
          {
            id: '1700',
            label: '川汇区',
            value: '411602'
          },
          {
            id: '1701',
            label: '扶沟县',
            value: '411621'
          },
          {
            id: '1702',
            label: '西华县',
            value: '411622'
          },
          {
            id: '1703',
            label: '商水县',
            value: '411623'
          },
          {
            id: '1704',
            label: '沈丘县',
            value: '411624'
          },
          {
            id: '1705',
            label: '郸城县',
            value: '411625'
          },
          {
            id: '1706',
            label: '淮阳县',
            value: '411626'
          },
          {
            id: '1707',
            label: '太康县',
            value: '411627'
          },
          {
            id: '1708',
            label: '鹿邑县',
            value: '411628'
          },
          {
            id: '1709',
            label: '项城市',
            value: '411681'
          }
        ]
      },
      {
        id: '1192',
        label: '驻马店市',
        value: '411700',
        children: [
          {
            id: '1711',
            label: '驿城区',
            value: '411702'
          },
          {
            id: '1712',
            label: '西平县',
            value: '411721'
          },
          {
            id: '1713',
            label: '上蔡县',
            value: '411722'
          },
          {
            id: '1714',
            label: '平舆县',
            value: '411723'
          },
          {
            id: '1715',
            label: '正阳县',
            value: '411724'
          },
          {
            id: '1716',
            label: '确山县',
            value: '411725'
          },
          {
            id: '1717',
            label: '泌阳县',
            value: '411726'
          },
          {
            id: '1718',
            label: '汝南县',
            value: '411727'
          },
          {
            id: '1719',
            label: '遂平县',
            value: '411728'
          },
          {
            id: '1720',
            label: '新蔡县',
            value: '411729'
          }
        ]
      },
      {
        id: '1193',
        label: '省直辖县级行政区划',
        value: '419000',
        children: [
          {
            id: '1721',
            label: '济源市',
            value: '419001'
          }
        ]
      }
    ]
  }
]

export default options
