/**
 * 没有权限控制的路由 直接加载控制
 */
import Layout from '@/layout/index.vue'
import addressBook from './constant/addressBook'
import jobManagement from './constant/jobManagement'
import aq from './constant/A&Q'

/**
 * 参数
 * hidden: true的话不会渲染到slideBar组件内
 * redirect: 当访问指定路由时--跳转到xx
 * alwaysShow: 单级路由树(展示二级路由树)
 * white: true 白名单 不需要登陆就能访问的
 */
const constantRoutes = [
    {
        path: '/',
        component: Layout,
        redirect: '/dashboard',
        // alwaysShow: false, // 不变成二级路由树
        name: 'dash',
        hidden: true,
        children: [
            {
                path: 'dashboard',
                component: () => import('@/views/dashboard/index.vue'),
                name: 'Dashboard',
                meta: { title: '首页', icon: 'House', hidden: true, affix: true }
            }
        ]
    },
    {
        path: '/login',
        component: () => import('@/views/login/loginPage.vue'),
        meta: { title: '登录', white: true, },
        name: 'login',
        hidden: true
    },
    {
        path: '/wxLogin',
        component: () => import('@/views/login/wxLogin.vue'),
        meta: { title: '登录', white: true, },
        name: 'wxLogin',
        hidden: true
    },
    // 已经有登录信息了 但是未绑定
    {
        // 
        path: '/bindPhoneNumber',
        component: () => import('@/views/login/bindPhoneNumber.vue'),
        meta: { title: '绑定手机号', white: true, },
        name: 'bindPhoneNumber',
        hidden: true
    },

    // 404 401等丢失页面不需要配置name, 如果设置了会被存放在tagsView中
    {
        path: '/404',
        name: '404',
        component: () => import('@/views/error-page/404.vue'),
        hidden: true
    },
    {
        path: '/401',
        name: '401',
        component: () => import('@/views/error-page/401.vue'),
        hidden: true
    },
    addressBook, jobManagement, aq
]

export default constantRoutes