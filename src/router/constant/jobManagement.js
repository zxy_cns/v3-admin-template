import Layout from '@/layout/index.vue'

const jobManagement = {
    path: '/jobManagement',
    component: Layout,
    name: 'job',
    meta: {
        title: '作业管理',
        icon: 'Memo', needCache: true
    },
    children: [
        {
            path: 'jobManagement',
            component: () => import('@/views/jobManagement/jobManagement.vue'),
            name: 'jobManagement',
            meta: { title: '作业管理', needCache: true, icon: 'Memo' }
        }
    ]
}

export default jobManagement
