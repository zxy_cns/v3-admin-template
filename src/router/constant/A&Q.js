import Layout from '@/layout/index.vue'

const A_Q = {
    path: '/aq',
    component: Layout,
    name: 'aq',
    meta: {
        title: '提问答复',
        icon: 'ChatDotRound'
    },
    children: [
        {
            path: 'A_Q',
            component: () => import('@/views/A&Q/A&Q.vue'),
            name: 'A_Q',
            meta: { title: '提问答复', icon: 'ChatDotRound' }
        }
    ]
}

export default A_Q
