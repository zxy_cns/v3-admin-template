import Layout from '@/layout/index.vue'

const addressBook = {
    path: '/addressBook',
    component: Layout,
    name: 'address',
    meta: {
        title: '群组管理',
        icon: 'Promotion'
    },
    children: [
        {
            path: 'addressBook',
            component: () => import('@/views/addressBook/addressBook.vue'),
            name: 'addressBook',
            meta: { title: '群组管理', icon: 'Promotion', needCache: true }
        },
    ]
}

export default addressBook
