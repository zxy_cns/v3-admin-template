import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
// no permission
import constantRoutes from './constantRoutes'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: constantRoutes
})

export default router
