/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 * 异步权限路由需要通过 权限验证遍历后 使用 router.add 插入到路由中
 */
const asyncRoutes = [
    /** when your routing map is too long, you can split it into small modules **/
    // 404 page must be placed at the end !!!
    { path: '*', name: '*', redirect: '/404', hidden: true }
]

export default asyncRoutes