import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './assets/main.css'
import ElementPlus from 'element-plus'
import globalProperties from './globalProperties'
import 'element-plus/dist/index.css'
// 权限控制器  
import './permission' // permission control
import { pinia } from './stores'
import '@/styles/index.scss' // global css
// 注册Icon组件
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
const app = createApp(App)
// 注册图标组件
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
// 全局属性挂载
globalProperties(app)
app.use(pinia).use(router).use(ElementPlus).mount('#app')

