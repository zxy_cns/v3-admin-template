import { R } from '@/utils/request'

// 获取班级table
export function getData(data) {
    return R({
        url: 'contacts/getPage',
        data
    })
}

// 创建
export function saveAddress(data) {
    return R({
        url: 'contacts/save', data
    })
}

// 编辑
export function updateAddress(data) {
    return R({
        url: 'contacts/update', data
    })
}

// 获取编辑详情
export function getAddressDetails(id) {
    return R({
        url: 'contacts/getById', data: { id }
    })
}

// 获取学生名单
// export function getStudentList(id) {
//     return R({
//         url: 'contacts/getById', data: { id }
//     })
// }

// 添加通讯录
export function addStuToAddressFun(data) {
    return R({
        url: 'contacts/saveBatch',
        data
    })
}