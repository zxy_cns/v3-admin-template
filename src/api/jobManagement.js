import { R } from '@/utils/request'

// 获取作业table
export function getData(data) {
    return R({
        url: 'homework/getPage',
        data
    })
}

// 获取作业详情
export function getDetails(id) {
    return R({
        url: 'homework/getById',
        data: { id }
    })
}

// 获取提交作业的学生
export function getSubmitList(hId) {
    return R({
        url: 'homework/getStudentList',
        data: {
            hId,
            status: 2
        }
    })
}

// 查看学生提交的作业详情记录
export function getStudentSubmitJobRecords(id) {
    return R({
        url: 'homework/getHomeworkUserDetailList',
        data: {
            id
        }
    })
}

// 字典 获取通讯录小组
export function getAddressList(data) {
    return R({
        url: 'contacts/getList',
        data
    })
}

// 发布作业
export function publishAjobFun(data) {
    return R({
        url: 'homework/save',
        data
    })
}

// 批改作业
export function submitJob(data) {
    return R({
        url: 'homework/updateHomeworkUserNew',
        data
    })
}