import { R } from '@/utils/request'
// 获取用户路由权限
export function getRoles(data = {}) {
    return R({
        url: '/roleMenu/getList',
        data
    })
}