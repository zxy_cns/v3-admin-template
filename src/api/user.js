import { R } from '@/utils/request'

// 普通登录
export const Login = (data = {}) => {
    return R({
        url: 'userLogin/teacherLogin',
        data
    })
}
// 微信登陆
export const wxLogin = (data = {}) => {
    return R({
        url: 'userLogin/callback',
        data
    })
}
// 获取图形验证码
export const getSendCodeFun = (data = {}) => {
    return R({
        url: 'userLogin/sendCode',
        data
    })
}
// 获取图形验证码
export const getCaptchaFun = (data = {}) => {
    return R({
        url: 'userLogin/getVerifyCode'
    })
}
// 获取微信登陆state表示
export const getState = () => {
    return R({
        url: 'userLogin/getState'
    })
}
// 获取短信验证码
export const getCode = (data = {}) => {
    return R({
        url: 'userLogin/sendCode',
        data
    })
}
// 绑定
export const bindPhone = data => {
    return R({
        url: 'userLogin/wxTLogin',
        data
    })
}


// 获取权限
export const getRoles = () => {
    return R({
        url: 'userLogin/role',
    })
}