import { R } from '@/utils/request'
// 课程下拉
export function getCourseList(data = {}) {
    return R({
        url: 'course/getList',
        data
    })
}

// 获取table参数
export function getData(data) {
    return R({
        url: 'chapterQuestions/getPage',
        data
    })
}

// 查看作业详情
export function getViewingDetails(id) {
    return R({
        url: 'chapterQuestions/getById',
        data: { id }
    })
}

// 回答提问 
export function answerQuestion(data) {
    return R({
        url: 'chapterQuestions/answer',
        data
    })
}
// 删除提问
export function deleteQuestion(id) {
    return R({
        url: 'chapterQuestions/deleteAnswer',
        data: { id }
    })
}
