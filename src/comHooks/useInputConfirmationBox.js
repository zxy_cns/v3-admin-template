import { ElMessageBox } from 'element-plus'
import { R } from '@/utils/request'

const useInputConfirmationBox = ({
    message,
    title = '操作提示',
    canceText = '取消',
    confirmText = '确认',
    inputPattern = /.*/,
    validateErrMsg = '检验失败',
    url = '',
    data = {},
    inputKey = 'value',
    isParams = false,
    callback, // 确认的回调
    catchCallbacl // 取消的回调
}) => {
    ElMessageBox.prompt(message, title, {
        confirmButtonText: confirmText,
        cancelButtonText: canceText,
        inputPattern,
        inputErrorMessage: validateErrMsg,
    })
        .then(({ value }) => {
            data[inputKey] = value
            R({
                url,
                data
            }, isParams).then(res => {
                if (!res.code) {
                    callback && callback()
                }
            })
        }).catch(() => {
            catchCallbacl && catchCallbacl()
        })
}

export default useInputConfirmationBox