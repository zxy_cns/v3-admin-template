import { ElMessageBox } from 'element-plus'
import { R } from '@/utils/request'

// 参数
// message （字符串or h模板）

const useConfirmationBox = ({
  message,
  title = '操作提示',
  canceText = '取消',
  confirmText = '确认',
  url,
  data,
  isParams = false,
  callback
}) => {
  return ElMessageBox({
    title,
    message,
    showCancelButton: true,
    confirmButtonText: confirmText,
    cancelButtonText: canceText,
    beforeClose: (action, instance, done) => {
      if (action === 'confirm') {
        instance.confirmButtonLoading = true
        instance.confirmButtonText = 'Loading...'
        // 开始请求操作
        R({
          url,
          data
        }, isParams).then(res => {
          done()
          instance.confirmButtonLoading = false
          if (!res.code) {
            callback && callback()
          }
        })
      } else {
        done()
      }
    },
  })
  // .then((action) => {})
}

export default useConfirmationBox