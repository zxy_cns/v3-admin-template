import router from './router'
import { ElMessage, ElMessageBox } from 'element-plus'
// 进度条
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style 
import getPageTitle from '@/utils/get-page-title'
import { getToken } from './utils/user'
import useStore from './stores'
import { isArray } from 'lodash-es'

NProgress.configure({ showSpinner: false }) // NProgress Configuration NProgress进度条的配置信息
// 白名单无需登录
const whiteList = ['/login', '/wxLogin'] // no redirect whitelist 不需要token验证的路由
// 特殊路由(全局提示路由例如：404，401，error等 主要防止跳转访问时候把非业务路由插入到tagsView中)
const special = ['/404', '/401']

router.beforeEach(async (to, from, next) => {
    const { userInfo, routerPermission, tags } = useStore()
    // 用户信息 用户权限
    const { getAsyncRoles, useRoles, loginOut } = userInfo
    // 获取路由
    const { generateRoutes, getRoles } = routerPermission
    // 进度条开始
    NProgress.start()
    // 跳转时 切换title
    document.title = getPageTitle(to.meta.title)
    const hasToken = getToken()

    if (hasToken) {
        if (to.path === '/login') {
            next({ path: '/' })
            NProgress.done()
        } else {
            try {
                // 如果没有router 没有 name 并且不是特殊路由
                if (!to.name && !special.includes(to.path)) {
                    console.error(`router ‘${to.path}’ is not special route, not set routerName`)
                }
                // 确认用户是否获取了路由权限 没有获取就去获取
                var roles, accessRoutes
                if (!(useRoles && useRoles.length > 0)) {
                    // 获取角色权限并根据角色获取可访问路由树
                    roles = await getAsyncRoles()
                    accessRoutes = await generateRoutes(roles)
                } else {
                    // 已经获取过直接读缓存
                    accessRoutes = getRoles
                }

                // 判断是否是特殊路由[404,401]等
                let isSpecial = special.includes(to.path)
                // 判断路由是否存在 不存在就动态添加到路由表里（vRouter4中不再支持批量插入）
                if (!router.hasRoute(to.name)) {
                    // 如果是特殊路由
                    if (isSpecial) {
                        next()
                    }
                    // 不是特殊路由并且name存在 就去查找指定路由并插入
                    navigationTo(next, to, accessRoutes)
                } else {
                    // 放行前判断是否是特殊路由 如果不是就插入tagsView
                    if (!isSpecial) {
                        tags.addTag(to)
                    }
                    // 放行
                    next()
                }
            } catch (error) {
                // 清空路由树
                await loginOut()
                ElMessage.error('权限获取失败')
                next("/login")
                NProgress.done()
            }
        }
    } else {
        /* token已经存在*/
        // if (whiteList.indexOf(to.path) !== -1) {
        // 路径在免登录白名单中直接放行
        if (to.meta.white) {
            next()
        } else {
            // 没有token跳转到登录页面
            next(`/login?redirect=${to.path}`)
            NProgress.done()
        }
    }
})

router.afterEach(() => {
    // finish progress bar
    NProgress.done()
})

// 返回当前需要跳转的路由对象
const generateRoute = (to, list) => {
    let index = 0;
    let flag = false;
    let toArr = to.path

    list.forEach((item, i) => {
        if (item.path == toArr) {
            console.log('匹配到了', item.path, toArr)
            index = i;
            flag = true;
            return
        }
        // 是由有子路由
        if (item.children && isArray(item.children)) {
            item.children.forEach((v) => {
                // 判断是否有动态路由
                if (v.path.indexOf("/:") != -1) {
                    if (to.path.indexOf(`${toArr}/${v.path.substr(0, v.path.indexOf("/:"))}`) != -1) {
                        index = i;
                        flag = true;
                        return
                    }
                } else {
                    if (to.path == `${toArr}/${v.path}`) {
                        index = i;
                        flag = true;
                        return
                    }
                }
            });
        }
    });

    return { route: list[index], flag };
};

const navigationTo = (next, to, routerList) => {
    // 在动态路由表中获取 当前需要跳转的路由对象
    const result = generateRoute(to, routerList);
    // result.flag 判断是否有匹配到的路由, false则跳转到404页面
    if (result.flag) {
        // 如果匹配到路由就存起来然后跳转进去
        router.addRoute(result.route);
        next({ ...to, replace: true })
    } else {
        console.log('没找到路由')
        // 如果是无权限就是401
        next({ path: "/404" })
    }
}

