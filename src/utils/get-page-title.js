import { Settings } from '@/settings'

const title = Settings.title || 'V3 admin'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}