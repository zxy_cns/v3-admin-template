import {
    SUCCESS_CODE,
    ERROR_CODE,
    NOT_LOGIN,
    ACCOUNT_NOT_EXIST
} from './code'
import router from '@/router'
import { ElMessage, ElMessageBox } from 'element-plus'
import useStore from "@/stores";

// code
// showErrorMsg 是否需要提示tootip
const DONT_NEED_TOOTIP_CODE = [NOT_LOGIN]
function httpInterception(response) {
    const { code, msg } = response

    if (!DONT_NEED_TOOTIP_CODE.includes(code) && code !== SUCCESS_CODE) {
        ElMessage({
            message: msg,
            type: 'error',
            duration: 5 * 1000
        });
    }
    // 处理事件
    EVENT[code] && EVENT[code]()
}

const EVENT = {
    [NOT_LOGIN]: notLogin,
    [ACCOUNT_NOT_EXIST]: accountNotExist,
}

// 未登录或令牌失效 403
function notLogin() {
    const { userInfo } = useStore()
    ElMessageBox.confirm('你已被登出，可以取消继续留在该页面，或者重新登录', '确定登出', {
        confirmButtonText: '重新登录',
        cancelButtonText: '取消',
        type: 'warning'
    }).then(() => {
        // 退出登录 
        userInfo.loginOut()
    }).catch(action => {
        // console.log('留在此页面')
    })
}

// 用户不存在 501
function accountNotExist() {
    const { userInfo } = useStore()
    userInfo.loginOut()
}

export default httpInterception