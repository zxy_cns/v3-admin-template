import axios from 'axios'
import { ElMessage, ElMessageBox } from 'element-plus'
import { getToken } from './user';
 
import httpInterception from './httpInterception';

// 创建axios实例
const request = axios.create({
  baseURL: window.IPPATH, // api的base_url
  timeout: 5000 // 请求超时时间
})

// request拦截器
request.interceptors.request.use(config => {
  const token = getToken()
  if (token) {
    // console.log('token =========> ', token)
    config.headers['token'] = token;
  }
  return config
}, error => {
  Promise.reject(error)
})

// respone拦截器
request.interceptors.response.use(
  response => {
    const res = response.data;
    // 拦截code状态处理
    httpInterception(res)
    return res
  },

  error => {
    // console.log('err' + error)// for debug
    ElMessage.error(error.message)
    // return Promise.reject(error)
  })

export default request

export function R(obj, from) {
  const settings = {
    method: 'POST',
    ...obj
  }
  // from ture 标识params参数
  settings[from ? 'params' : 'data'] = obj.data || {}
  return request(settings)
}
