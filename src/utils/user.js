import Cookies from 'js-cookie'
const UserinfoKey = 'V3_u_info'

// 如果没有存储、返回空对象 
export function getUserinfo() {
  const U = Cookies.get(UserinfoKey) || '{}'
  return JSON.parse(U)
}

export function setUserinfo(userinfo) {
  Cookies.set(UserinfoKey, JSON.stringify(userinfo))
}

export function getToken() {
  return getUserinfo()?.token
}

export function removeUserinfo() {
  Cookies.remove(UserinfoKey)
}

// 清除所有cookie和缓存记录
export function removeAllCookiesAndLocalStorage() {
  // 清除加密cookie存储记录
  const cookies_store = Cookies.get()
  for (const cookieKey in cookies_store) {
    Cookies.remove(cookieKey)
  }
  // 清除localStorage持久化记录
  localStorage.clear()
}
