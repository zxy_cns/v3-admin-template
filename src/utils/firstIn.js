import Cookies from 'js-cookie'
const firstInKey = 'v0-3-welcome'

export const getFirstIn = () => {
    // 如果没有值表示是第一次进入
    return Cookies.get(firstInKey) || null
}

export const setFirstIn = () => {
    Cookies.set(firstInKey, 'in')
}
