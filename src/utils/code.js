// 请求成功
export const SUCCESS_CODE = 0
// 服务器异常
export const ERROR_CODE = -9999
// 未登录
export const NOT_LOGIN = 403
// 用户不存在
export const ACCOUNT_NOT_EXIST = -501