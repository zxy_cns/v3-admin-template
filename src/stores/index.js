import { createPinia } from 'pinia'
import useUserInfo from './modules/userInfo'
import usePermission from './modules/permission'
import useTags from './modules/tags'

// 使用持久化插件
import piniaPluginPersist from 'pinia-plugin-persist'
import useSystem from './modules/system'

// 持久化
const store = createPinia()
store.use(piniaPluginPersist)
export const pinia = store

// 统一导出useStore方法
export default function useStore() {
    return {
        userInfo: useUserInfo(),
        routerPermission: usePermission(),
        tags: useTags(),
        useSystem: useSystem()
    }
}