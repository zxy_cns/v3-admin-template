import { defineStore } from "pinia"
// 异步权限路由
import asyncRoutes from "@/router/asyncRoutes"
// 无权限路由
import constantRoutes from "@/router/constantRoutes"

/**
 * Use meta.role to determine if the current user has permission
 * 判断当前角色是否有此路由得权限
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
    if (route.meta && route.meta.roles) {
        return roles.some(role => route.meta.roles.includes(role))
    } else {
        return true
    }
}

/**
 * Filter asynchronous routing tables by recursion
 * 通过递归过滤异步路由表
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, roles) {
    const res = []

    routes.forEach(route => {
        const tmp = { ...route }
        if (hasPermission(roles, tmp)) {
            if (tmp.children) {
                tmp.children = filterAsyncRoutes(tmp.children, roles)
            }
            res.push(tmp)
        }
    })

    return res
}

const usePermission = defineStore('routerPermission', {
    // 开启数据持久化
    // persist: {
    //     enabled: true,
    //     strategies: [
    //         {
    //             key: 'routerPermission',
    //             storage: localStorage,
    //             // paths: ['list']
    //         }
    //     ]
    // },
    state: () => ({
        // 路由表（静态+动态集合）
        routes: [],
        // 要插入路由表的异步路由表（筛选的动态）
        addRoutes: [],
    }),
    getters: {
        getRoles: state => state.routes,
    },
    actions: {
        setRouter(routes) {
            this.addRoutes = routes
            this.routes = constantRoutes.concat(routes)
            // console.log('this.routes ========> ', this.routes)
        },
        // 根据权限刷新路由
        async generateRoutes(roles) {
            // const useUinfo = useUserInfo()
            // console.log('useUinfo ===> ', useUinfo)
            return new Promise(resolve => {
                let accessedRoutes
                // console.log('roles ===> ', roles)
                // 如果登陆的用户权限中包含admin权限 就直接访问全部
                if (roles.includes('admin')) {
                    accessedRoutes = asyncRoutes || []
                } else {
                    // 否则过滤掉固定的路由
                    accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
                }
                // 
                this.setRouter(accessedRoutes)
                resolve(this.routes)
            })
        }
    }
})

export default usePermission 