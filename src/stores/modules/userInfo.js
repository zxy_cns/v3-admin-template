import { defineStore } from "pinia"
import { getUserinfo, setUserinfo } from '@/utils/user'
import { getToken } from '@/utils/user'
import router from '@/router'
import { getRoles, Login, wxLogin } from '@/api/user'
import { removeAllCookiesAndLocalStorage } from "@/utils/user"

export const useUserInfo = defineStore('userInfo', {
    state: () => ({
        uInfo: getUserinfo(),
        roles: [],
    }),
    getters: {
        useUserData: state => state.uInfo,
        useRoles: state => state.roles,
        // 获取token (本地token与store二取一)
        getUserDataToken: state => {
            const loaclToken = getToken()
            const uToken = state.token
            if (!loaclToken && !uToken) {
                console.warn(`token is null`)
            }
            return loaclToken || uToken || null
        }
    },
    actions: {
        setUserData(data) {
            this.uInfo = data
            setUserinfo(data)
        },
        setRoles(roles) {
            this.roles = roles
        },
        // 获取用户登录信息
        async getAsyncUser(data, { type, callback }) {
            // wxLogin
            let LoginAsync
            if (type == 'phone') {
                // 手机号登录
                LoginAsync = Login(data)
            } else {
                // 微信登录
                LoginAsync = wxLogin(data)
            }
            // 返回出去
            return LoginAsync.then(res => {
                // console.log('res ===userInfo . ', res)
                if (!res.code) {
                    // 保存用户状态
                    this.setUserData(res.data.userInfo)
                    debugger
                    if (type == 'phone') {
                        router.replace('/')
                    } else {
                        // 微信登陆（因为是回调嵌套所以不能使用router.push）
                        // 如果微信扫码没有token信息表示没有绑定用户手机号
                        if (getToken()) {
                            // 有token
                            const href = window.location.href.split('#')[0] + '#/'
                            window.parent.location.href = href
                        } else {
                            // 如果没有token执行回调
                            callback && callback(res)
                        }
                    }
                } else {
                    // 报错了执行回调
                    callback && callback(res)
                }
            })
        },
        // 绑定手机号成功
        bindPhoneSuccess(res) {
            // 保存用户状态
            this.setUserData(res.data.userInfo)
        },
        // 获取用户得路由权限
        getAsyncRoles() {
            return new Promise((resolve, reject) => {
                getRoles().then(res => {
                    const { code, data: roles, msg } = res
                    // console.log('code ======> ', res)
                    if (code) {
                        reject(msg)
                        return
                    }
                    // 获取roles权限 
                    // 角色必须是非空数组
                    if (!roles || roles.length <= 0) {
                        reject('getInfo: roles must be a non-null array!')
                    }
                    // 存到本地
                    // console.log('roles =====> ', roles)
                    this.setRoles(roles)
                    resolve(roles)
                }).catch(error => {
                    reject(error)
                })
            })
        },
        // 退出登录
        loginOut() {
            return new Promise(resolve => {
                removeAllCookiesAndLocalStorage()
                router.replace('/login')
                resolve()
            })
        },
        // 退出登录
    }
})

export default useUserInfo