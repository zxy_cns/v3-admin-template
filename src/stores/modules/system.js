// 系统配置
import { defineStore } from 'pinia'

const useSystem = defineStore('window_system', {
    persist: {
        enabled: true,
        strategies: [
            {
                key: 'window_system',
                storage: localStorage,
            }
        ]
    },
    state: () => {
        return {
            collapse: true,  // 左侧导航是否展开
        }
    },

    actions: {
        setCollapse(value) {
            this.collapse = value
        }
    }
})

export default useSystem