// pinia持久化测试
import { defineStore } from 'pinia'

const useUserStore = defineStore('user', {
    // 开启数据持久化
    // 使用该插件，开启数据缓存
    persist: {
        //这里存储默认使用的是session
        enabled: true,
        strategies: [
            {
                //key的名称
                key: 'user',
                //更改默认存储，我更改为localStorage
                storage: localStorage,
                // 白名单 可以选择哪些进入local存储 默认是全部进去存储
                // paths: ['list']
            }
        ]
    },
    state: () => {
        return {
            name: 'zs',
            age: 100,
        }
    },

    actions: {
        setName() {
            this.name = 'xsxsxs'
        }
    }
})

export default useUserStore