import { has } from 'lodash'
import { defineStore } from 'pinia'

const useTags = defineStore('tags', {
    // 开启数据持久化
    // 使用该插件，开启数据缓存
    persist: {
        //这里存储默认使用的是session
        enabled: true,
        strategies: [
            {
                key: 'tags',
                storage: localStorage,
            }
        ]
    },
    state: () => ({
        tagView: [],
        // 缓存的路由(routerlink 缓存)
        cacheRouter: []
    }),
    getters: {
        getTags(state) {
            // 排序
            state.tagView = state.tagView.sort(a => !a.meta.affix)
            return state.tagView
        },
    },

    actions: {
        addTag(router) {
            // 存放tags 
            // hidden不存在 or hidden false 
            if (!this.tagView.some(v => v.path === router.path)) {
                // 如果hidden存在并且为true 不存放tags
                const hidden = has(router.meta, 'hidden') && router.meta.hidden
                if (hidden) return
                // 存放tags
                this.tagView.push(
                    Object.assign({}, router, {
                        title: router.meta.title || 'no-name'
                    })
                )
            }

            // 缓存路由 (needCache == true)
            const needCache = has(router.meta, 'needCache') && router.meta.needCache
            if (needCache && !this.cacheRouter.some(name => name === router.name)) {
                // 只存储name
                this.cacheRouter.push(router.name)
            } else {
                // if (this.cacheRouter.some(name => name === router.name)) {
                //     console.log("已缓存")
                // }
                // if (!needCache) {
                //     console.log("不需要缓存")
                // }
            }
        },
        delTags(router) {
            // 删除
            for (const [i, v] of this.tagView.entries()) {
                if (v.path === router.path) {
                    this.tagView.splice(i, 1)
                    break
                }
            }
        }
    }
})

export default useTags